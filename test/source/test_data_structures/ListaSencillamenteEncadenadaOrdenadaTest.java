/**
 * Tomado de N9 (APO 2 Honores) 
 * Creador de los JUnit tests:  Christian Camilo Aparicio Baquen 
 */

package test_data_structures;

import model.data_structures.ListaSencillamenteEncadenadaOrdenada;

public class ListaSencillamenteEncadenadaOrdenadaTest extends ListaAbstractaOrdenadaTest 
{

	@Override
	public void setupEscenario1() {
		super.setupEscenario1();
		lista = new ListaSencillamenteEncadenadaOrdenada<>(comparador, true);
	}
}
