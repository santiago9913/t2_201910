/**
 * Tomado de N9 (APO 2 Honores) 
 * Creador de los JUnit tests:  Christian Camilo Aparicio Baquen 
 */

package test_data_structures;

import model.data_structures.ListaDobementeEncadenada;

public class ListaSencillamenteEncadenadaTest extends ListaAbstractaNoOrdenadaTest 
{

	@Override
	public void setupEscenario1() 
	{
		lista = new ListaDobementeEncadenada<>();		
	}

	@Override
	public void testListIterator() {
		//No se probar� en esta prueba.
	}
}
