/**
 * Tomado de N9 (APO 2 Honores) 
 * Creador de los JUnit tests:  Christian Camilo Aparicio Baquen 
 */

package test_data_structures;

import model.data_structures.ListaDobementeEncadenada;

public class ListaDoblementeEncadenadaTest extends ListaAbstractaNoOrdenadaTest 
{

	@Override
	public void setupEscenario1() {
		lista = new ListaDobementeEncadenada<>();
	}
	
}
