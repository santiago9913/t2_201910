package controller;


import java.io.File;

import api.IMovingViolationsManager;
import model.data_structures.ListaDobementeEncadenada;
import model.logic.MovingViolationsManager;
import model.vo.VOMovingViolations;

public class Controller {
	
	private static final String CSV_PATH = "./data/infracciones.csv"; 

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IMovingViolationsManager  manager = new MovingViolationsManager();
	
	
	
	public static void loadMovingViolations() {
		
		 manager.loadMovingViolations(CSV_PATH);
		
	}
	
	public static ListaDobementeEncadenada <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		return manager.getMovingViolationsByViolationCode(violationCode); 
	}
	
	public static ListaDobementeEncadenada <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		return manager.getMovingViolationsByAccident(accidentIndicator);
	}
}
