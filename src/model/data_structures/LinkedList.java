package model.data_structures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ListIterator;

public class LinkedList<E> extends ListaEncadenadaAbstracta<E>
{

	/**
	 * Constante de serializaci�n.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Construye la lista vac�a.
	 * <b>post: </b> Se ha inicializado el primer nodo en null
	 */
	public LinkedList() 
	{
		//TODO Completar el m�todo de acuerdo a la documentaci�n.

		primerNodo = null; 
		cantidadElementos = 0; 
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�metro. Actualiza el n�mero de elementos.
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public LinkedList(E nPrimero) throws NullPointerException
	{
		//TODO Completar el m�todo de acuerdo a la documentaci�n.

		if(nPrimero == null) {
			throw new NullPointerException("El elemento a agregar es nulo");
		}
		else {
			primerNodo = new NodoListaSencilla<E>(nPrimero);
			cantidadElementos = 1; 
		}

	}

	/**
	 * Agrega un elemento al final de la lista, actualiza el n�mero de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param elem el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public boolean add(E elemento) throws NullPointerException
	{
		// TODO Completar seg�n la documentaci�n

		boolean puede = false; 

		if(elemento == null) {
			throw new NullPointerException("El elemento a agregar es nulo");
		}
		else {
			if(primerNodo == null) {
				primerNodo = new NodoListaSencilla<E>(elemento); 
				cantidadElementos++; 
				puede = true; 
			}
			else {
				if(contains(elemento)) {
					return puede = false;
				}
				else {
					NodoListaSencilla<E> actual = primerNodo; 
					while(actual.darSiguiente() != null) {
						actual = actual.darSiguiente(); 
					}
					actual.cambiarSiguiente(new NodoListaSencilla<E>(elemento));
					cantidadElementos++;  
					puede = true; 
				}
			}
		}
		return puede;
	}

	/**
	 * Agrega un elemento en la posici�n dada de la lista. Todos los elementos siguientes se desplazan.
	 * Actualiza la cantidad de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param pos la posici�n donde se desea agregar. Si pos es igual al tama�oo de la lista se agrega al final
	 * @param elem el elemento que se desea agregar
	 * @throws IndexOutOfBoundsException si el inidice es < 0 o > size()
	 * @throws NullPointerException Si el elemento que se quiere agregar es null.
	 */
	public void add(int index, E elemento) 
	{
		int contador = 0; 
		// TODO Completar seg�n la documentaci�n
		if(index < 0 || index > size()) {
			throw new IndexOutOfBoundsException("El indice esta por fuera del rango");
		}
		if(elemento == null) {
			throw new NullPointerException("El elemento es nulo"); 
		}
		NodoListaSencilla<E> nuevo = new NodoListaSencilla<E>(elemento);

		if(!contains(nuevo)) {
			if(primerNodo != null) {
				if(index == 0) {
					nuevo.cambiarSiguiente(primerNodo);
					primerNodo = nuevo;
					cantidadElementos++;
				}
				else if(index == size()) {
					NodoListaSencilla<E> ultimo = primerNodo;
					while(ultimo.darSiguiente() != null) {
						ultimo = ultimo.darSiguiente();
					}
					ultimo.cambiarSiguiente(nuevo);
					cantidadElementos++;
				}
				else if (index < size() && index > 0) {
					NodoListaSencilla<E> ultimo = primerNodo;
					NodoListaSencilla<E> anterior = null;
					boolean encontrado = false;
					while(ultimo.darSiguiente() != null && !encontrado) {
						if(index == contador) {
							nuevo.cambiarSiguiente(ultimo);
							anterior.cambiarSiguiente(nuevo);
							cantidadElementos++;
							encontrado = true;
						}
						else {
							anterior = ultimo;
							ultimo = ultimo.darSiguiente();
							contador++;
						}
					}
				}
			}
			else {
				primerNodo = nuevo;
				cantidadElementos++;
			}
		}
	}


	@Deprecated
	public ListIterator<E> listIterator() 
	{
		throw new UnsupportedOperationException ();
	}

	@Deprecated
	public ListIterator<E> listIterator(int index) 
	{
		throw new UnsupportedOperationException ();
	}


	/**
	 * Elimina el nodo que contiene al objeto que llega por par�metro. Actualiza el n�mero de elementos.
	 * @param objeto el objeto que se desea eliminar. objeto != null
	 * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
	 */
	@SuppressWarnings("unchecked")
	public boolean remove(Object objeto) 
	{
		boolean elim = false; 

		if(primerNodo != null) {
			if(primerNodo.darSiguiente() != null) {
				if(primerNodo.darElemento().equals(objeto)) {
					primerNodo = primerNodo.darSiguiente(); 
					cantidadElementos--; 
					elim = true; 
				}
				else {
					NodoListaSencilla<E> anterior = primerNodo; 
					while(anterior.darSiguiente() != null && !elim) {
						if(anterior.darSiguiente().darElemento().equals(objeto)) {
							anterior.cambiarSiguiente(anterior.darSiguiente().darSiguiente());
							cantidadElementos--; 
							elim = true; 
						}
						else {
							anterior = anterior.darSiguiente(); 
						}
					}
					if(anterior.darElemento().equals(objeto)) {
						anterior = null; 
						cantidadElementos--; 
						elim = true; 
					}
				}
			}
			else {
				if(primerNodo.darElemento().equals(objeto)) {
					clear();
					elim = true; 
				}
			}
		}

		return elim;
	}

	/**
	 * Elimina el nodo en la posici�n por par�metro. Actualiza la cantidad de elementos.
	 * @param pos la posici�n que se desea eliminar
	 * @return el elemento eliminado
	 * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
	 */
	public E remove(int pos) throws IndexOutOfBoundsException
	{
		//TODO Completar seg�n la documentaci�n

		E buscado = null; 

		if(pos < 0 || pos >= size()) {
			throw new IndexOutOfBoundsException("El indice se encuentra por fuera del rango"); 
		}
		else {
			buscado = get(pos);
			remove(buscado); 
		}

		return buscado; 
	}

	/**
	 * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro. La cantidad de elementos se actualiza.
	 * @param coleccion la colecci�n de elementos a mantener. coleccion != null
	 * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
	 */
	public boolean retainAll(Collection<?> coleccion) 
	{
		//TODO Completar seg�n la documentaci�n
		boolean modificado = false; 
		if(primerNodo != null) { 
			for(int i = 0; i < size(); i++) {
				if(!coleccion.contains(get(i))) {
					remove(i); 
					modificado = true; 
					i--;
				}
			}
		}
		return modificado;
	}

	/**
	 * Crea una lista con los elementos de la lista entre las posiciones dadas
	 * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
	 * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
	 * @return una lista con los elementos entre las posiciones dadas
	 * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
	 */

	public List<E> subList(int inicio, int fin) 
	{
		//TODO Completar seg�n la documentaci�n
		
		if((inicio < 0) || (fin > size()) || (fin < inicio)) {
			throw new IndexOutOfBoundsException("Los indices se encuentran fuera del rango"); 
		}
		List<E> sub = new ArrayList<>(); 
		for(int i = inicio; i < fin; i++) {
			sub.add(get(i)); 
		}
		
		return sub; 
	}

}
