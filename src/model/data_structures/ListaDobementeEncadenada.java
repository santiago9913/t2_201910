package model.data_structures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Clase que representa la lista doblemente encadenada
 * @param <E> Tipo de los objetos que almacenar� la lista.
 */
public class ListaDobementeEncadenada<E> extends ListaEncadenadaAbstracta<E>
{

	/**
	 *  Constante de serializaci�n
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Construye una lista vacia
	 * <b>post:< /b> se ha inicializado el primer nodo en null
	 */
	public ListaDobementeEncadenada() 
	{
		primerNodo = null;
		cantidadElementos = 0;
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public ListaDobementeEncadenada(E nPrimero)
	{
		if(primerNodo == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		primerNodo = new NodoListaDoble<E>(nPrimero);
		cantidadElementos = 1;
	}

	/**
	 * Agrega un elemento al final de la lista
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id.
	 * Se actualiza la cantidad de elementos.
	 * @param e el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public boolean add(E e) 
	{
		// TODO Completar seg�n la documentaci�n
		
		boolean puede = false; 

		if(e == null) {
			throw new NullPointerException("El elemento a agregar es nulo");
		}
		else {
			if(primerNodo == null) {
				primerNodo = new NodoListaDoble<E>(e); 
				cantidadElementos++; 
				puede = true; 
			}
			else {
				if(contains(e)) {
					return puede = false;
				}
				else {
					NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo; 
					NodoListaDoble<E> nuevo = new NodoListaDoble<E>(e); 
					while(actual.darSiguiente() != null) {
						actual = (NodoListaDoble<E>) actual.darSiguiente(); 
					}
					nuevo.cambiarAnterior(actual);
					actual.cambiarSiguiente(nuevo);
					cantidadElementos++;  
					puede = true; 
				}
			}
		}
		return puede;
	}
	

	/**
	 * Agrega un elemento al final de la lista. Actualiza la cantidad de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
	 * @param elemento el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public void add(int index, E elemento)
	{
		boolean agregado = false; 
		if(index < 0 || index > size()) {
			throw new IndexOutOfBoundsException("El indice esta por fuera del rango");
		}
		if(elemento == null) {
			throw new IndexOutOfBoundsException("El elemento a agregar es nulo"); 
		}
		NodoListaDoble<E> nuevo = new NodoListaDoble<E>(elemento);
		if(primerNodo != null) {
			NodoListaDoble<E> actual = (NodoListaDoble<E>) primerNodo;
			if(index == 0) {
				nuevo.cambiarSiguiente(actual);
				actual.cambiarAnterior(nuevo);
				primerNodo = nuevo;
				agregado = true; 
				cantidadElementos++; 
			}
			else {
				while(!agregado && actual != null && actual.darSiguiente() != null) {
					if(indexOf(actual.darSiguiente()) == index && !contains(nuevo)) {
						nuevo.cambiarSiguiente(actual.darSiguiente());
						actual.cambiarSiguiente(nuevo);
						nuevo.cambiarAnterior(actual);
						agregado = true; 
						cantidadElementos++; 
					}
					else {
						actual  = (NodoListaDoble<E>) actual.darSiguiente();
					}
				}
				if(!agregado) {
					actual.cambiarSiguiente(nuevo);
					nuevo.cambiarAnterior(actual);
					cantidadElementos++; 
				}
			}
		}
		else if(index == 0) {
			primerNodo = new NodoListaDoble<E>(elemento);
			cantidadElementos++; 
		}	
	}

	/**
	 * M�todo que retorna el iterador de la lista.
	 * @return el iterador de la lista.
	 */
	public ListIterator<E> listIterator() 
	{
		return new IteradorLista<E>((NodoListaDoble<E>) primerNodo);
	}

	/**
	 * M�todo que retorna el iterador de la lista desde donde se indica.
	 * @param index �ndice desde se quiere comenzar a iterar.
	 * @return el iterador de la lista.
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public ListIterator<E> listIterator(int index) 
	{
		if(index< 0 || index >= size())
			throw new IndexOutOfBoundsException("El �ndice buscado est� por fuera de la lista.");
		return new IteradorLista<E>((NodoListaDoble<E>) darNodo(index));
	}

	/**
	 * Elimina el nodo que contiene al objeto que llega por par�metro.
	 * Actualiza la cantidad de elementos.
	 * @param objeto el objeto que se desea eliminar. objeto != null
	 * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
	 */
	@SuppressWarnings("unchecked")
	public boolean remove(Object o) 
	{
		// TODO Completar seg�n la documentaci�n
		
		boolean elim = false; 

		if(primerNodo != null) {
			if(primerNodo.darSiguiente() != null) {
				if(primerNodo.darElemento().equals(o)) {
					primerNodo = primerNodo.darSiguiente(); 
					cantidadElementos--; 
					elim = true; 
				}
				else {
					NodoListaDoble<E> anterior = (NodoListaDoble<E>) primerNodo; 
					while(anterior.darSiguiente() != null && !elim) {
						if(anterior.darSiguiente().darElemento().equals(o)) {
							anterior.cambiarSiguiente(anterior.darSiguiente().darSiguiente());
							cantidadElementos--; 
							elim = true; 
						}
						else {
							anterior = (NodoListaDoble<E>) anterior.darSiguiente(); 
						}
					}
					if(anterior.darElemento().equals(o)) {
						anterior = null; 
						cantidadElementos--; 
						elim = true; 
					}
				}
			}
			else {
				if(primerNodo.darElemento().equals(o)) {
					clear();
					elim = true; 
				}
			}
		}

		return elim;
	}

	/**
	 * Elimina el nodo en la posici�n por par�metro.
	 * Actualiza la cantidad de elementos.
	 * @param pos la posici�n que se desea eliminar
	 * @return el elemento eliminado
	 * @throws IndexOutOfBoundsException si index < 0 o index >= size()
	 */
	public E remove(int index) 
	{
		// TODO Completar seg�n la documentaci�n
		
		E buscado = null; 

		if(index < 0 || index >= size()) {
			throw new IndexOutOfBoundsException("El indice se encuentra por fuera del rango"); 
		}
		else {
			buscado = get(index);
			remove(buscado); 
		}

		return buscado; 
	}

	/**
	 * Deja en la lista s�lo los elementos que est�n en la colecci�n que llega por par�metro.
	 * Actualiza la cantidad de elementos
	 * @param coleccion la colecci�n de elementos a mantener. coleccion != null
	 * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
	 */
	public boolean retainAll(Collection<?> c) 
	{
		// TODO Completar seg�n la documentaci�n
		
		boolean modificado = false;
		if(primerNodo != null) {
			for(int i = 0; i < size(); i++) {
				if(!c.contains(get(i))) {
					remove(i); 
					modificado = true;
					i--;
				}
			}
		}
		return modificado;
	}

	/**
	 * Crea una lista con los elementos de la lista entre las posiciones dadas
	 * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
	 * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
	 * @return una lista con los elementos entre las posiciones dadas
	 * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
	 */
	public List<E> subList(int inicio, int fin) 
	{
		// TODO Completar seg�n la documentaci�n
		
		if((inicio < 0) || (fin > size()) || (fin < inicio)) {
			throw new IndexOutOfBoundsException("Los indices se encuentran fuera del rango"); 
		}
		List<E> sub = new ArrayList<>(); 
		for(int i = inicio; i < fin; i++) {
			sub.add(get(i)); 
		}
		
		return sub; 

	}

}
