package model.data_structures;

import java.util.Collection;
import java.util.Comparator;

/**
 * Estructura de datos lista ordenada por algun criterio de ordenamiento de E
 * @author Christian
 *
 * @param <E> Tipo de dato que se almacenar� en la lista, debe ser Unicamente identificado
 */
public class ListaSencillamenteEncadenadaOrdenada<E> extends LinkedList<E>
{

	/**
	 * Constante de Serializaci�n.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Comparador de E, permite comparar dos E para mantener el orden en la lista.
	 */
	private Comparator<E> comparador;

	/**
	 * Indica si la lista debe organizarse ascendente o descendentemente
	 */
	private boolean ascendente;

	/**
	 * Construye una lista vac�a.
	 * <b>post: </b> - Se ha inicializado el primer nodo en null. <br/>
	 * - Se ha inicializado el criterio de comparaci�n por el que se ordenar� el elemento.
	 * - Se ha inicializado si se quiere ordenar ascendente.
	 * @param comparador Criterio de comparaci�n por el que se ordenar�n los elementos en la lista.
	 */
	public ListaSencillamenteEncadenadaOrdenada(Comparator<E> comparador, boolean ascendente)
	{
		this.comparador = comparador;
		this.ascendente = ascendente;
	}

	/**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�metro. Actualiza el n�mero de elementos.
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @param comparador Criterio de comparaci�n por el que se ordenar�n los elementos en la lista.
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public ListaSencillamenteEncadenadaOrdenada(E nPrimero, Comparator<E> comparador, boolean ascendente)
	{
		super(nPrimero);
		this.comparador = comparador;
		this.ascendente = ascendente;
	}


	/**
	 * Agrega un elemento a la lista manteniendo el orden de acuerdo al criterio de comparaci�n, actualiza el n�mero de elementos.
	 * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id.
	 * @param elem el elemento que se desea agregar.
	 * @return true en caso que se agregue el elemento o false en caso contrario. 
	 * @throws NullPointerException si el elemento es nulo
	 */
	public boolean add(E elemento) throws NullPointerException
	{
		//TODO Completar seg�n la documentaci�n.

		boolean puede = false; 
		if(elemento == null) {
			throw new NullPointerException("El elemento es nulo"); 
		}
		NodoListaSencilla<E> nuevo = new NodoListaSencilla<E>(elemento); 
		NodoListaSencilla<E> actual = primerNodo;
		if(primerNodo == null) {
			primerNodo = nuevo;
			actual = primerNodo; 
			cantidadElementos++; 
			puede = true; 
		}
		else {
			NodoListaSencilla<E> siguiente = actual.darSiguiente();
			NodoListaSencilla<E> anterior = null; 
			
			if(ascendente == true) {
				while(siguiente != null && comparador.compare(elemento, actual.darElemento()) > 0) {
					if(actual.darElemento().equals(elemento)) {
						return puede; 
					}
					else {
						anterior = actual;
						actual = siguiente; 
						siguiente = siguiente.darSiguiente();
					}
				}
				if(comparador.compare(elemento, primerNodo.darElemento()) < 0) {
					NodoListaSencilla<E> temp = primerNodo;
					actual = nuevo;
					actual.cambiarSiguiente(temp);
					primerNodo = actual;
					anterior = null; 
					siguiente = primerNodo.darSiguiente();
					cantidadElementos++; 
					puede = true; 
				}
				else if(siguiente == null && comparador.compare(elemento, actual.darElemento()) > 0) {
					siguiente = nuevo; 
					actual.cambiarSiguiente(siguiente);
					cantidadElementos++; 
					puede = true; 
				}
				else if(comparador.compare(elemento, actual.darElemento()) < 0) {
					NodoListaSencilla<E> temp = actual; 
					actual = nuevo;
					actual.cambiarSiguiente(temp);
					anterior.cambiarSiguiente(actual);
					cantidadElementos++; 
					puede = true; 
				}
			}
			else {
				while(siguiente != null && comparador.compare(elemento, actual.darElemento()) < 0) {
					if(actual.darElemento().equals(elemento)) {
						return puede;
					}
					else {
						anterior = actual;
						actual = siguiente;
						siguiente = siguiente.darSiguiente();
					}
				}
				if(comparador.compare(elemento, primerNodo.darElemento()) > 0) {
					NodoListaSencilla<E> temp = primerNodo;
					actual = nuevo;
					actual.cambiarSiguiente(temp);
					primerNodo = actual;
					anterior = null; 
					siguiente = primerNodo.darSiguiente();
					cantidadElementos++; 
					puede = true; 
				}
				else if(siguiente == null && comparador.compare(elemento, actual.darElemento()) < 0) {
					siguiente = nuevo; 
					actual.cambiarSiguiente(siguiente);
					cantidadElementos++; 
					puede = true; 
				}
				else if(comparador.compare(elemento, actual.darElemento()) > 0) {
					NodoListaSencilla<E> temp = actual; 
					actual = nuevo;
					actual.cambiarSiguiente(temp);
					anterior.cambiarSiguiente(actual);
					cantidadElementos++; 
					puede = true; 
				}
			}
		}
		return puede; 
	}




	//------------------------------------------------------------------------------------------
	// M�todos no soportados por la lista (No los soporta porque no tienen sentido, la lista siempre debe estar organizada por el criterio de comparaci�n).
	//-----------------------------------------------------------------------------------------
	@Override
	@Deprecated
	public void add(int index, E elemento) 
	{
		throw new UnsupportedOperationException("No se puede hacer uso de esta operaci�n en este tipo de lista");
	}

	@Override
	@Deprecated
	public E set(int index, E element) throws IndexOutOfBoundsException 
	{
		throw new UnsupportedOperationException("No se puede hacer uso de esta operaci�n en este tipo de lista");
	}

	@Override
	@Deprecated
	public boolean addAll(int pos, Collection<? extends E> c) 
	{
		throw new UnsupportedOperationException("No se puede hacer uso de esta operaci�n en este tipo de lista");
	}

}
