package model.vo;

import model.logic.MovingViolationsManager;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations {

	private int objectId, totalPaid;
	
	private String location, ticketIssueDate, accidentalIndicator, violationDescription; 
	
	public VOMovingViolations(int objectId, int totalPaid, String location, String ticketIssueDate, String accidentalIndicator, String violationDescription) {
		this.objectId = objectId;
		this.totalPaid = totalPaid; 
		this.location = location; 
		this.ticketIssueDate = ticketIssueDate; 
		this.accidentalIndicator = accidentalIndicator; 
		this.violationDescription = violationDescription; 
	}
	
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentalIndicator;
	}
		
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription; 
	}
}
