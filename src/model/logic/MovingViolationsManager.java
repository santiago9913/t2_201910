package model.logic;

import java.io.FileReader;
import java.util.Iterator;
import java.util.List;

import javax.print.DocFlavor.STRING;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import api.IMovingViolationsManager;
import model.vo.VOMovingViolations;
import test_data_structures.ListaDoblementeEncadenadaTest;
import model.data_structures.ListaDobementeEncadenada;


public class MovingViolationsManager implements IMovingViolationsManager {

	public ListaDobementeEncadenada<String> allData; 

	/**
	 * Constructor
	 */
	public MovingViolationsManager() {
		allData = new ListaDobementeEncadenada<String>(); 
	}


	public void loadMovingViolations(String movingViolationsFile){
		// TODO Auto-generated method stub
		try {
			FileReader fileReader = new FileReader(movingViolationsFile); 
			CSVReader csvReader = new CSVReaderBuilder(fileReader).withSkipLines(1).build(); 
			List<String[]> data =  csvReader.readAll(); 
			for(String[] row : data) {
				for(String cell : row) {
					allData.add(cell); 
					System.out.println(cell);
				}
				System.out.println();
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}


	}
	@Override
	public ListaDobementeEncadenada <VOMovingViolations> getMovingViolationsByViolationCode (String violationCode) {
		// TODO Auto-generated method stub
		ListaDobementeEncadenada<VOMovingViolations> datos = new ListaDobementeEncadenada<VOMovingViolations>(); 
		for(int i = 0; i < allData.size(); i++){
			String[] data = allData.get(i).split(";");
			if(data[5].equals(violationCode)){
				datos.add(new VOMovingViolations(Integer.parseInt(data[0]), Integer.parseInt(data[2]), data[1], data[4], data[3], data[6])); 
			}
			
		}
		return datos; 

	}

	@Override
	public ListaDobementeEncadenada <VOMovingViolations> getMovingViolationsByAccident(String accidentIndicator) {
		// TODO Auto-generated method stub
		
		ListaDobementeEncadenada<VOMovingViolations> datos = new ListaDobementeEncadenada<VOMovingViolations>(); 
		for(int i = 0; i < allData.size(); i++) {
			String[] data = allData.get(i).split(";"); 
			if(data[3].equals(accidentIndicator)) {
				datos.add(new VOMovingViolations(Integer.parseInt(data[0]), Integer.parseInt(data[2]), data[1], data[4], data[3], data[6])); 
			}
		}
		
		return datos; 
	}	


}
